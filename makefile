CPPFLAGS=-std=c++17 -Wall 
CC=g++
CATCH2FLAGS=--success --use-colour yes
TEST1=[succ]
TEST2=[notsucc]
TEST3=[strange]


all:    src/main.cpp src/search.hpp
	$(CC) $(CPPFLAGS) -o bin/search.out src/main.cpp  
	

run:    #bin/search.out  ## No such target!!
	@bin/search.out 

tests:  bin/tests.out 
	@bin/tests.out $(CATCH2FLAGS)

test1: bin/tests.out 
	@bin/tests.out $(TEST1) $(CATCH2FLAGS)

test2: bin/tests.out 
	@bin/tests.out $(TEST2) $(CATCH2FLAGS)

test3: bin/tests.out 
	@bin/tests.out $(TEST3) $(CATCH2FLAGS)


bin/tests.out: bin/tests_main.o tests/tests.cpp tests/tests_main.cpp src/search.hpp src/main.cpp
	@printf "\033[36mCompiling Tests...\n\033[0m"	
	$(CC) $(CPPFLAGS)  -o bin/tests.out bin/tests_main.o tests/tests.cpp

bin/tests_main.o: tests/tests_main.cpp 
	@printf "\033[36mCompiling Test Driver...(please be patient)\n\033[0m"	
	$(CC) $(CPPFLAGS)  -c tests/tests_main.cpp -o bin/tests_main.o


clean:
	@printf "\033[31mRemoving objects (and temporary files)\033[0m\n"
	@rm -rf bin/*.o*

